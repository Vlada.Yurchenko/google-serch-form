import axios from 'axios';

const SEARCH_URL = 'https://api.jikan.moe/v3/search/anime';
const MAX_LIST_SIZE = 10;

function api(url) {
  const getList = (queryParam) => axios.get(url, { params: { q: queryParam, limit: MAX_LIST_SIZE } });

  return {
    getList
  };
}

export default api(SEARCH_URL);
