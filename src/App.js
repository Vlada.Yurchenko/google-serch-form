import LogoGoogle from './assets/images/goole-logo.png';
import { Header, SearchForm } from './components';
import './App.css';

function App() {
  return (
    <div className='app'>
      <Header/>
      <div className='logo-container'>
        <img src={LogoGoogle} alt='google-logo'></img>
      </div>
      <SearchForm/>
    </div>
  );
}

export default App;
