import { FaSearch } from 'react-icons/fa';
import './style.css';

function List({ list }) {
  return (
    <div className='list-container'>
      { 
        list?.map(({ title, id }) => <li className='list-group-item' key={id}>
        <FaSearch className='search-icon' size={12}/>{title}</li>)
      }
    </div>
  )
}

export default List;