import menuImg from '../../assets/images/menu.jpg';
import './style.css';

function Header() {
  return (
    <header className='header'>
      <nav>
        <ul className='nav_bar'>
          <li className='nav-link'><a href='#'>Gmail</a></li>
          <li className='nav-link'><a href='#'>Images</a></li>
          <li className='nav-link nav-link--menu'>
            <a href='#'>
              <div className='icon'>
                <img src={menuImg}/>
              </div>
            </a>
          </li>
          <li className='nav-link nav-link--profile'>
            <a href='#'>
              <div className='icon'>V</div>
            </a>
          </li>
        </ul>  
      </nav>  
    </header>  
  )
}

export default Header;