import { useState } from 'react';
import { useDebouncedCallback } from 'use-debounce';
import { MdKeyboardVoice } from 'react-icons/md';
import { FaSearch } from 'react-icons/fa';
import List from '../list';
import api from '../../api';
import './style.css';

function SearchForm(){
  const [searchValue, setSearchValue] = useState('');
  const [searhList, setSearchList] = useState(null);

  const getData = useDebouncedCallback(() => {
    api.getList(searchValue)
    .then(({ data: { results } }) => {
      setSearchList(results);
    })
    .catch(() => {
      setSearchList(null);
    });
  }, 500)

  return (
    <>
      <div class='form-outline col-4'>
        <div className={`search-container ${searhList ? 'active': ''}`}>
          <button className='btn btn-search'>
            <FaSearch className='search-icon'/>
          </button>
          <input
            type='search'
            className='form-control'
            placeholder='Search Google or type a URL'
            value={searchValue}
            onChange={({ target }) => {
              setSearchValue(target.value);
              getData();
            }}
            aria-label='Search'
            />
          <button className='btn voice-search-button' title='Search by voice'>
            <MdKeyboardVoice size={20} className='voice-icon'/>
          </button>
        </div>
        <List list={searhList}/>
      </div>
    </>
  )
}

export default SearchForm;
